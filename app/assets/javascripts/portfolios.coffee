# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# set variables
ready = undefined
set_positions = undefined
file_browse_click = undefined
file_browse_change = undefined

set_positions = ->
  $('.card').each (i) ->
    $(this).attr "data-pos", i + 1
    return
  return

file_browse_click = ->
  $('.browse').on 'click', ->
    file = $(this).parent().parent().parent().find('.file')
    file.trigger 'click'
    return

file_browse_change = ->
  $('.file').on 'change', ->
    $(this).parent().find('.form-control').val $(this).val().replace(/C:\\fakepath\\/i, '')
    return

# set ready function
ready = ->
  # get file browse click feature
  file_browse_click()
  # get file browse change feature
  file_browse_change()
  # get portfolio_item positions
  set_positions()

  # init sortable from html5sortable library to DOM el
  $('.sortable').sortable()
  $('.sortable').sortable().bind "sortupdate", (e, ui) ->
    updated_order = []
    # call positions function to obtain updated values and set to array
    set_positions()
    $('.card').each (i) ->
      updated_order.push
        id: $(this).data("portfolio-id")
        position: i + 1
      return

    # send position data to rails dynamically
    $.ajax
      type: 'PUT'
      url: '/portfolios/sort'
      data: order: updated_order
    return
  return

# when document loads, get ready sortable function
$('document').ready ready