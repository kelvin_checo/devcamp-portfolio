module BlogsHelper
	def gravatar_helper(user)
		image_tag "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(user.email)}", width: 40
	end

	class CodeRayify < Redcarpet::Render::HTML
		def block_code(code, language)
			CodeRay.scan(code, language).div
		end
	end

	def markdown(text)
		coderayified = CodeRayify.new(filter_html: true, hard_wrap: true)

		options = {
			fenced_code_blocks: true,
			no_intra_emphasis: true,
			autolink: true,
			lax_html_blocks: true
		}

		markdown_to_html = Redcarpet::Markdown.new(coderayified, options)
		markdown_to_html.render(text).html_safe
	end

	def status_admin_action(blog_obj)
		if blog_obj.published?
			link_to fa_icon('toggle-on'), toggle_status_blog_path(blog_obj), title: "Switch to Draft", class: "nav-link status-toggle"
		else
			link_to fa_icon('toggle-off'), toggle_status_blog_path(blog_obj), title: "Switch to Published", class: "nav-link status-toggle"
		end
	end
end
