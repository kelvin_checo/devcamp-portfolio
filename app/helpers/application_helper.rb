module ApplicationHelper
  def login_helper(css_class = '')
    if current_user.is_a?(GuestUser)
      (link_to "Sign Up", new_user_registration_path, class: css_class) +
      " ".html_safe +
      (link_to "Login", new_user_session_path, class: css_class)
    else
      link_to "Logout", destroy_user_session_path, method: :delete, class: css_class
    end
  end

  def source_helper(css_class='')
    if session[:source]
      greeting = "#{ fa_icon('info-circle') } Thanks for visiting me from #{session[:source]}, please feel free to #{ link_to 'Contact Me', contact_path} if you'd like to work together."
      content_tag(:div, greeting.html_safe, class: css_class)
    end
  end

  def copyright_generator
    DevcampViewTool::Renderer.copyright "Kelvin Checo", "All rights reserved"
  end

  def nav_items
    [
      {
        url: root_path,
        title: 'Home'
      },
      {
        url: about_path,
        title: 'About'
      },
      {
        url: blogs_path,
        title: 'Blog'
      },
      {
        url: portfolios_path,
        title: 'Portfolio'
      },
      {
        url: tech_news_path,
        title: 'Tech News'
      },
      {
        url: contact_path,
        title: 'Contact'
      }
    ]
  end

  def nav_generator(css_class, html_tag_start = '', html_tag_end = '')
    nav_links = ''

    nav_items.each do |item|
      nav_links << "#{html_tag_start}<a href='#{item[:url]}' class='#{css_class} #{active_page?(item[:url])}'>#{item[:title]}</a>#{html_tag_end}"
    end

    nav_links.html_safe
  end

  def active_page?(path)
    "active" if current_page? path
  end

  def alerts
    if flash[:alert]
      alert_generator(flash[:alert], "Alert")
    elsif flash[:error]
      alert_generator(flash[:error], "Error")
    else
      alert_generator(flash[:notice])
    end
  end

  def alert_generator(msg, type="")
    js add_gritter(msg, :title => type, :sticky => false)
  end
end
