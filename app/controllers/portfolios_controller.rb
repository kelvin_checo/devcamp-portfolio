class PortfoliosController < ApplicationController
	before_action :set_portfolio_item, only: [:show, :edit, :update, :destroy]
	layout "portfolio"
	access all: [:show, :index], user: {except: [:sort, :destroy, :new, :create, :update, :edit]}, site_admin: :all

	def index
		@portfolio_items = Portfolio.by_position
	end

	def sort
		params[:order].each do |key, value|
			Portfolio.find(value[:id]).update!(position: value[:position])
		end

		render nothing: true
	end

	def new
		@portfolio_item = Portfolio.new
	end

	def create
    @portfolio_item = Portfolio.new(portfolio_params)

    respond_to do |format|
      if @portfolio_item.save
        format.html { redirect_to portfolios_path, notice: 'Your portfolio item was successfully created.' }
        #format.json { render :show, status: :created, location: @portfolio_items }
      else
        format.html { render :new }
        #format.json { render json: @portfolio_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    #redirect to index page
    respond_to do |format|
      if @portfolio_item.update(portfolio_params)
        format.html { redirect_to portfolios_path, notice: 'Record was successfully updated.' }
        #format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        #format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def destroy
    #destroy the record
    @portfolio_item.destroy

    #redirect to index page
    respond_to do |format|
      format.html { redirect_to portfolios_path, notice: 'Portfolio item has been deleted'}
      #format.json {head :no_content}
    end
  end

  private
		# Use callbacks to share common setup or constraints between actions.
		def set_portfolio_item
			#perform lookup
	    @portfolio_item = Portfolio.find(params[:id])
		end

    # Never trust parameters from the scary internet, only allow the white list through.
    def portfolio_params
      params.require(:portfolio).permit(:title,
                                        :subtitle,
                                        :body,
																				:main_image,
																				:thumb_image,
                                        technologies_attributes: [:id, :name, :_destroy]
                                        )
    end
end
