module DefaultPageContent
  extend ActiveSupport::Concern

  included do
    before_action :set_page_defaults
  end

  def set_page_defaults
    @website_name = "DevcampPortfolio"
    @page_name = "My Portfolio Website"
    @page_title = @website_name + " | " + @page_name
    @seo_keywords = "Kelvin Checo's portfolio"
  end
end
